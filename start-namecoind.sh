#!/bin/bash
#
# Description:
# Start Namecoin daemon
# To disable logging to file, set -nodebuglogfile
# To enable logging to file, set -debuglogfile=$LOG_FILE
#
# Authors:
# Jose G. Faisca <jose.faisca@gmail.com>
#

LOG_FILE="/tmp/namecoind.log"
namecoind -daemon -debuglogfile=$LOG_FILE
sleep 2
pgrep namecoind